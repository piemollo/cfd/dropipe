// ====== ====== Header
cout << "#===== ====== ====== ====== =====#"	<< endl;
cout << "|  Generate profil settings " 			<< endl;
cout << "|" << endl;

// ------ Initialization
verbosity=0;

// ------ path
load "msh3"
include "getARGV.idp"
string meshid = getARGV("-mesh","../mesh/vascular_pipe_1.mesh");
string outpath= getARGV("-out",   "");

// ------ Display settings
cout << "| #----- Settings:" 		<< endl;
cout << "|   +--- Path" 			<< endl;
cout << "|    Mesh Id : "+ meshid	<< endl;
cout << "|    Results : "+ outpath	<< endl;

// ------ Mesh
mesh3 Th = readmesh3(meshid);
fespace Vh(Th,P2);
fespace Qh(Th,P1);
fespace Ch(Th,P0);

// ------ Labels
// wall   : 1
// input  : 2
// output : 3

macro Wall()1//
macro InL()2//
macro OutL()3//

int[int] IFLab = [InL];
int[int] OFLab = [OutL];

// Extract bound label
varf vSetConstBnd(u,v) = int3d(Th)(u*v*0) + on(InL, u=1);
real[int] SetConstBnd = vSetConstBnd(0, Vh, tgv=1);

// Compute flowrate
varf vFlowX(u,v) = int2d(Th,InL)(v*N.x) + on(Wall, u=0);
varf vFlowY(u,v) = int2d(Th,InL)(v*N.y) + on(Wall, u=0);
varf vFlowZ(u,v) = int2d(Th,InL)(v*N.z) + on(Wall, u=0);
real[int] FX = vFlowX(0, Vh, tgv=0);
real[int] FY = vFlowY(0, Vh, tgv=0);
real[int] FZ = vFlowZ(0, Vh, tgv=0);

// Initialize
Vh ux = x, uy = y, uz = z;
int[int] Bidx(SetConstBnd.sum);
real[int,int] Bflow(SetConstBnd.sum,3);
real[int,int] Bcoord(SetConstBnd.sum,3);

int Nidx = 0;
for(int q=0; q<Vh.ndof; q++){
	if( SetConstBnd[q] == 1 ){
		Bidx[Nidx]      = q;
		Bcoord(Nidx, 0) = ux[][q];
		Bcoord(Nidx, 1) = uy[][q];
		Bcoord(Nidx, 2) = uz[][q];
		Bflow(Nidx, 0)  = FX[q];
		Bflow(Nidx, 1)  = FY[q];
		Bflow(Nidx, 2)  = FZ[q];
		Nidx++;
	}
}
Nidx--;

// --- Save to dat
if(outpath!=""){
	ofstream ofl(outpath+"InputBoundSettings.dat");
	ofl.precision(16);
	for(int q=0; q<Nidx; q++){
		ofl << Bidx[q] << " ";
		ofl << Bcoord(q,0) << " " << Bcoord(q,1) << " " << Bcoord(q,2);
		ofl << " ";
		ofl << Bflow(q,0)  << " " << Bflow(q,1)  << " " << Bflow(q,2);
		ofl << endl;
	}
}

cout << "#===== ====== ====== ====== =====#" << endl;
