cout << "#===== ====== ====== ====== =====#" 	<< endl;
cout << "|  Steady Stokes: Direct solver" 		<< endl;
cout << "|" << endl;

// ------ ------ Initiate
// ------ Library
verbosity=0;
include "getARGV.idp"
include "./module/ffmatlib/ffmatlib.idp"
include "./module/windkessel/windkessel3d.idp"
load "msh3"
load "iovtk"
load "MUMPS"

// ------ Settings
bool disp = usedARGV("-disp")!=-1;
bool savedat = usedARGV("-savedat")!=-1;
bool savevtu = usedARGV("-savevtu")!=-1;
string meshid = getARGV("-mesh","./mesh/vascular_pipe_1.mesh");
string outpath= getARGV("-out","");

// ------ Mesh
mesh3 Th = readmesh3(meshid);

// ------ Labels
int wall   = 1;	//wall
int inlet  = 2;	//left side
int outlet = 3;	//right side

// ------ Physics
real mu   = getARGV("-mu",  3.5e-6);
real rho  = getARGV("-rho", 1e-6);
real nu   = mu/rho;

// --- Resistance
real rd = getARGV("-rd",1e-16);
real rp = getARGV("-rp",0.0);
real cp = getARGV("-cp",1e16);
real lp = getARGV("-lp",0.0);
real icp= 1./cp;
real wcp= rp + rd;

// --- Input and Adim.
real[int,int] input(0,0);
ffLoadArray(input,"./inflow/inflow.dat");

// ------ Input profile
fespace Vh(Th,P2);
Vh lp1, lp2, lp3;
real[int,int] ProfileData(0,0);
ffLoadArray(ProfileData, "./profile/InputProfile.dat");
lp2=0.;
lp1=0.;
lp3=0.;
for(int q=0; q<ProfileData.n; q++)
 	lp1[][ProfileData(q,0)] = ProfileData(q,1);
lp1[] *= -1;

// ------ scaling
real Area  = int2d(Th, inlet)(1.);
real flow  = input(0,1);
real UU    = flow/Area;
real gamma = UU;
flow /= gamma;

// ------ ------ Problem
// ------ FE spaces
fespace Yh(Th, [P2,P2,P2,P1]);
macro def(u,p) u#1,u#2,u#3,p //
Yh [def(u,p)];

// ------ Weak formulation
varf Stokes(def(u,p), def(v,q))
	= int3d(Th)(
		  mu * gamma * (
			  dx(u1)*dx(v1) + dy(u1)*dy(v1) + dz(u1)*dz(v1)
			+ dx(u2)*dx(v2) + dy(u2)*dy(v2) + dz(u2)*dz(v2)
			+ dx(u3)*dx(v3) + dy(u3)*dy(v3) + dz(u3)*dz(v3)
		)
		- p*dx(v1) - p*dy(v2) - p*dz(v3)
		- q*dx(u1) - q*dy(u2) - q*dz(u3)
	)
	// Dirichlet conditions
	+ on(inlet,
		u1=lp1*flow,
	 	u2=lp2*flow,
	 	u3=lp3*flow )
	+ on(wall, u1=0, u2=0, u3=0)
	;

// --- Windkessel implicit resistance
matrix Wh;
WindkesselMatrix(Th, outlet, Wh);

// ------ Algebraic formulation
matrix StokesLHS = Stokes(Yh,Yh);
StokesLHS       += gamma*wcp*Wh;
//set(StokesLHS, solver=UMFPACK);
set(StokesLHS, solver=sparsesolver);

real[int] StokesRHS = Stokes(0, Yh);
u1[] = StokesLHS^-1 * StokesRHS;

// ------ Rescaling
[def(u,p)] = [def(gamma*u,p)];


// ------ ------ Save
if(outpath!=""){
	// ------ Save VTU
	int[int] fforder = [1,1];
	if(savevtu){
		savevtk(outpath+"SteadyStokes.vtu", 
			Th, [u1,u2,u3], p, 
			dataname="Velocity Pressure",
			order=fforder);
	}
	
	// ------ Save dat
	if(savedat){
		ofstream ofl(outpath+"SteadyStokes.dat");
		ofl << u1[];
	}
}

// ------ ------ Display
// ------ Results
if(disp){
	plot([u1,u2,u3], value=1, wait=1, cmm="Velocity field");
	plot(p, value=1, fill=true, wait=1, cmm="Pressure field");
}

// ------ Computed factors
real dP   = abs(  int2d(Th,inlet)(p)/Area 
				- int2d(Th,outlet)(p)/Area );
real outP = int2d(Th,outlet)(p)/Area;
real Q    = abs(int2d(Th,inlet)( N.x*u1 + N.y*u2 + N.z*u3 ));
real v0   = int2d(Th,inlet)(abs(u1)+abs(u2)+abs(u3))/Area;
real EqRh = dP/Q;
real[int] absu = abs(u1[]);

// ------ Physics info
cout << "| # Mesh: "   +meshid	<< endl;
cout << "|     flow = "+ input(0,1);
cout << " (Scaled:"+flow+")"	<< endl;
cout << "|       mu = "+ mu   	<< endl;
cout << "|      rho = "+ rho  	<< endl;
cout << "|" << endl;
cout << "| # Save: "+outpath	<< endl;
cout << "|     .vtu = "+ savevtu << endl;
cout << "|     .dat = "+ savedat << endl;
cout << "|" << endl;
cout << "| # Computed: "		<< endl;
cout << "|      dP  = "+ dP		<< endl;
cout << "|       Q  = "+ Q		<< endl;
cout << "|     v0   = "+ v0		<< endl;
cout << "|     vmax = "+ absu.max	<< endl;
cout << "|  out Pre.= "+ outP 	<< endl;
cout << "|  ext.res.= "+ (rp+rd)<< endl;
cout << "|    Eq Rh = "+ EqRh	<< endl;
cout << "|" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;
