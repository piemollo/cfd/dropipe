#!/bin/bash

gmsh ./vascular_pipe.geo -3 -o ./vascular_pipe_1.mesh -clscale 0.2
gmsh ./vascular_pipe.geo -3 -o ./vascular_pipe_2.mesh -clscale 0.1
gmsh ./vascular_pipe.geo -3 -o ./vascular_pipe_3.mesh -clscale 0.08
