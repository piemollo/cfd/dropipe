# Mesh of Vascular pipe

The default mesh used here represents a simple circular straight pipe
describe using its length $L$ and its radius $R$.
Here it is used to mimic typical size of a superior sagittal sinus
leading to the choices of 
$L=75[{\rm mm}]$ 
and 
$R=3[{\rm mm}]$.

![](/asset/mesh/pipe_scheme.png)

In order to make comparison, the bash script `generate_meshes.sh` 
produces 3 different meshes of decreasing step size.
Meshes construction is based on [`gmsh`](https://gmsh.info/) and 
can be done simply calling:
```bash
./generate_mesh.sh
```

Illustration of the finest generated mesh:

![](/asset/mesh/pipe_mesh.png)