from pylab import *
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "mathplazo"
})

# Set time step
freq           = 0.8;
ncycle         = 5;
step_per_cycle = 201;
nstep          = 1 + (step_per_cycle-1)*ncycle;
T              = linspace(0.0, ncycle*freq, nstep);

# Compute inflow
F = ( 5700 - 500*cos(2*pi*(1.0/freq)*T) );

# Export
fl = open("inflow.dat","w");

for i in range(nstep):
    fl.write("{:8.6f} {:8.2f}\n".format(T[i],F[i]));

fl.close()

# Draw
fg = figure(1,figsize=(6,2),constrained_layout=True)

plot(T,F)

title("Inlet flow rate")
xlabel("Time $[s]$")
ylabel("Inflow $[mm^3.s^{-1}]$")

grid(True, linestyle=':')
yticks(linspace(min(F), max(F), 5))

#show()
fg.savefig("../assets/inflow.pdf");
