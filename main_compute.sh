#!/bin/bash

# --- Set path
# ffpath=.../
export ffpath="$HOME/Soft/FreeFem-413/install/bin";
export off=" -nw -ne -v 0 -glut $ffpath/ffglut ";
export xff="$ffpath/FreeFem++ $off ";

export n_proc="2";
export mff="$ffpath/ff-mpirun -n ${n_proc} ";


# --- Settings
# Mesh
export mesh="./mesh/vascular_pipe_3.mesh";

# Profile
export profile=0.5

# Windkessel
export wdksl=" -rp 8.26e-6 -rd 1e-16 ";                     # R
# export wdksl=" -rp 6.26e-6 -rd 2e-6 -cp 3e5 -lp 0.0 ";     # RCR
# export wdksl=" -rp 6.26e-6 -rd 2e-6 -cp 3e5 -lp 8.26e-7 "; # RCRL

# Path
export snappath="./results/R/"; # R
# export snappath="./results/RCR/";     # RCR
# export snappath="./results/RCRL/";    # RCRL
mkdir -p ${snappath};
date >> ${snappath}log.txt

# --- Steady Stokes init
$xff Stokes.edp \
	-mesh $mesh \
	-input ./inflow/inflow.dat \
	-profile ${profile} \
	-out ${snappath} \
	${wdksl} \
	-savevtu -savedat \
	>> ${snappath}log.txt

# --- Time trajectory
$mff NavierStokes.edp -ne \
	-mesh $mesh \
	-input ./inflow/inflow.dat \
	-ssinit ${snappath}SteadyStokes.dat \
	-profile ${profile} \
	-out ${snappath} \
	${wdksl} \
	-savevtu \
	>> ${snappath}log.txt
