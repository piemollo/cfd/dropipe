import pylab as pl
import numpy as np

pl.rcParams.update({
    "text.usetex": True,
    "font.family": "mathplazo"
})

# --- Load
pre_R   = np.loadtxt("../results/R/pressure_out.dat")
pre_RCR = np.loadtxt("../results/RCR/pressure_out.dat")
pre_RCL = np.loadtxt("../results/RCRL/pressure_out.dat")
inflow  = np.loadtxt("../inflow/inflow.dat")

# --- Settings
rp = 6.26e-6
rd = 2e-6
cp = 3e4
lp = 8.26e-7
R  = rd + rp

pre_Rt = inflow[:,1]*R # Theoritical fully resistive pressure
TT     = inflow[:,0];

# --- Display
fg = pl.figure(1,figsize=(6,2),constrained_layout=True)
pl.clf()
pl.grid(True, linestyle=':')

pl.plot(TT, pre_R)
pl.plot(TT, pre_RCR)
pl.plot(TT, pre_RCL)
pl.plot(TT, pre_Rt,  '--')

pl.legend(['R','RCR','RCRL', 'R theo.'])
pl.xlabel('Time [$s$]')
pl.ylabel("Pressure $[kg.mm^{-1}.s^{-2}]$")

#pl.show()
fg.savefig("../assets/pressure_ext.pdf");
